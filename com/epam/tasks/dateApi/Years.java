package com.epam.tasks.dateApi;

import java.time.*;

public class Years {
    public static void main(String[] args){
        LocalDateTime oldLocalDateTime = LocalDateTime.of(2007, Month.MAY, 01, 7, 7, 33);
        LocalDateTime nowLocalDateTime = LocalDateTime.now();

        Duration duration = Duration.between(oldLocalDateTime, nowLocalDateTime);
        Period p = Period.between(oldLocalDateTime.toLocalDate(), nowLocalDateTime.toLocalDate());
        System.out.println("You are " + p.getYears() + " years, or " + p.toTotalMonths() +
                " months or " + duration.toDays() +
                " days old or " + duration.toHours() +
                " hours old or " + duration.toMinutes() +
                " minutes old or " + duration.toMillis() +
                " millis old.");
    }
}
