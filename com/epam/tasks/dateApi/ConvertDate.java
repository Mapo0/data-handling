package com.epam.tasks.dateApi;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class ConvertDate {


    private String getFormatDateUS(String date) {

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd MMMM yyyy", Locale.US);
        LocalDate localDate = LocalDate.parse(date, dateTimeFormatter);
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("dd/MMM/yy", Locale.US);
        return localDate.format(timeFormatter);
    }



    public static void main(String[] args) {
        String dateUS = "17 December 1995";
        System.out.println(new ConvertDate().getFormatDateUS(dateUS));
    }
}
