package com.epam.tasks.regEx;


public class Username {
    public static void main(String[] args) {
        String s = "Уважаемый, $userName, извещаем вас о том, что на вашем счете $paymentAccount " +
                "скопилась сумма, превышающая стоимость $numberMonths месяцев пользования нашими услугами. \n" +
                "Деньги продолжают поступать. Вероятно, вы неправильно настроили автоплатеж. С уважением, " +
                "$employee $userPosition";
        s = replaceStr(s, "userName", "Уважаемый Марат");
        s = replaceStr(s, "paymentAccount", "122434454546");
        s = replaceStr(s, "numberMonths", "12");
        s = replaceStr(s, "employee", "ФИО сотрудника");
        s = replaceStr(s, "userPosition", "Должность ");
        System.out.println(s);
    }

    private static String replaceStr(String s, String templateKey, String templateValue) {
        s = s.replaceAll("\\$" + templateKey + "\\,?", templateValue);
        return s;
    }
}

