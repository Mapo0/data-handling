package com.epam.tasks.regEx;


public class Number {

    public static void main(String[] args) {
        String str = "+7 (3412) 51-76-47";
        String pattern = "^\\+?([78]).\\(?3412\\)?.";
        str = str.replaceAll(pattern, "");
        System.out.println(str);

        String str1 = "8 3412 90-41-90";
        str1 = str1.replaceAll(pattern, "");
        System.out.println(str1);

        String str2 = "8 (3412) 4997-12";
        str2 = str2.replaceAll(pattern, "");
        System.out.println(str2);
    }
}
