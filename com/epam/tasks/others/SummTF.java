package com.epam.tasks.others;

import java.util.Objects;

public class SummTF {
    private boolean checkSum(double a, double b, double sum){
        return Objects.equals(Double.sum(a, b), sum);
    }
    public static void main(String[] args){
        double a = 0.25;
        double b = 0.3;
        double sum = 0.5;
        System.out.print(new SummTF().checkSum(a,b,sum));
    }
}
